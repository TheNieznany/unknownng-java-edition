This file is here, so that Git won't ignore this folder.

This folder is for external `.jar` files, but *unknownNG* isn't using any.

However, my build script (in `build.xml`) needs this.

Maybe I'll change it someday, maybe not.