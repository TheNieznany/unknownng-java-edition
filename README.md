# unknownNG Java Edition

A 2D game framework/engine thing made by The Nieznany.

## Build and run

*unknownNG Java Edition* uses Apache Ant for the building process.
This project doesn't have an entrypoint on its own---you need to provide a `main` method.

- `ant` - for build and run.
- `ant doc` - to generate the documentation.

---

## Introduction

First of all, since *unknownNG* is using Java Swing built-in library, we need to set up a `JFrame`. Feel free to change the window title passed to the `JFrame`'s constructor.

Next, it's time to use the first object in *unknownNG* --- the `GraphicsWrapper` class. In this example we'll set it to have 320x240 resolution, and the scale to 2.

Now let's set up our class that's derived from `Scene` --- `ExampleScene`. The base class is derived from `JPanel` (which comes from Swing library), so we can add it to the `window`, then configure it a bit more, and start the scene.

Below is an example of the main file. It contains the `Main` class, which is the entry point of the program.

```java
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import unknownng.*;

public class Main {
	private static void initialize() {
		JFrame window = new JFrame("unknownNG example");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);

		GraphicsWrapper gw = new GraphicsWrapper(320, 240, 2);
		ExampleScene exampleScene = new ExampleScene(gw);

		window.add(exampleScene);
		window.addKeyListener(exampleScene);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				initialize();
			}
		});
	}
}
```

And this is an example of a class derived from `Scene`.

```java
import unknownng.*;

public class ExampleScene extends Scene {
	// Put objects here.

	public ExampleScene(GraphicsWrapper gw) {
		super(gw);

		// Set up the objects here.
	}

	@Override
	public void update() {
		// Update the objects here.
	}

	@Override
	public void draw() {
		// And draw the objects here.
	}
}
```



---

## Examples

### How to get keyboard input from user?

Just implement `KeyListener` interface (from Java AWT) to your class derived from `Scene` (from *unknownNG*). See the example below:

```java
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import unknownng.*;

public class ExampleScene extends Scene implements KeyListener {
	// Put objects here.

	public ExampleScene(GraphicsWrapper gw) {
		super(gw);

		// Set up the objects here.
	}

	@Override
	public void update() {
		// Update the objects here.
	}

	@Override
	public void draw() {
		// And draw the objects here.
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
			// Handle pressed keys here.
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
			// Handle released keys here.
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// You probably won't use it, but you need to override it anyway.
	}
}
```

---

### How to make an `AnimatedSprite` object?

First of all, we need an image that contains the animation frames. For each animation, the frames need to be arranged side by side horizontally, and have the same size. You can put multiple animation frame sets in one image. Look at the example image below, that's more or less how it should be:

![An example image that has properly arranged animation frames to work with unknownNG.](./example_images/example1.png "example1")

After preparing the assets, it's time to code! Most of these lines should go in the scene's constructor.

```java
player = new AnimatedSprite(gw, new Sprite("images/player.png"), 16, 16);
```

Next, we'll create a new AnimatedSprite object. We pass the reference to `GraphicsWrapper` and a new `Sprite` with the filepath to our image. We also need to pass the width and height of a single frame.

```java
Rectangle initialRect = new Rectangle(0, 0, 16, 16);
```

Now is the time to set up the animations. We need to tell the `Animation` constructor where the first frame is located on the image. Also, it's used to calculate other frames' boundaries. This data is used by `AnimatedSprite`'s `draw()` function, in order to display the correct frame on the screen.

Since the first frame of the first animation is at the upper-left corner, `x` and `y` of `initialRect` both need to be set to `0`. We need width and height too, and that's 16x16 pixels for each frame.

```java
player.addAnimation("walk_left", new Animation(initialRect, 4, 0.25));
```

We're ready to save the first animation. Let's say that we have two animations --- the first one shows the player walking left, and the other walking right. Just to clarify, the animation names can be anything you want.

Of course we pass the arguments --- our `initialRect`, amount of frames present in this animation, and duration of each frame in seconds. (So it's one quarter of a second here.)

```java
initialRect.y = 16;
player.addAnimation("walk_right", new Animation(initialRect, 4, 0.5));
```

Since the second animation is below the first one, we can reuse the `initialRect` to make it point to the first frame of the next animation.

This time it's going to be called *walk_right*, and the frame duration is going to be set at half a second.

```java
player.playAnimation("walk_right");
```

Finally, we'll play *walk_right* animation. That's why we have those names.

```java
player.draw(new Point(10, 10));
```

This member function will display the current animation frame onto the screen. We need to pass a `Point` for the position.

```java
import unknownng.*;
import unknownng.AnimatedSprite.Animation;

public class ExampleScene extends Scene {
	private AnimatedSprite player;

	public ExampleScene(GraphicsWrapper gw) {
		super(gw);

		player = new AnimatedSprite(gw, new Sprite("images/player.png"), 16, 16);

		Rectangle initialRect = new Rectangle(0, 0, 16, 16);
		player.addAnimation("walk_left", new Animation(initialRect, 4, 0.25));

		initialRect.x = 16;
		player.addAnimation("walk_right", new Animation(initialRect, 4, 0.5));

		player.playAnimation("walk_right");
	}

	@Override
	public void update() {}

	@Override
	public void draw() {
		Point point = new Point(
			(GraphicsWrapper.screenWidth / 2) - (player.width / 2),
			(GraphicsWrapper.screenHeight / 2) - (player.height / 2)
		);

		player.draw(point);
	}
}
```

And here is the final example inside a scene. Just a heads-up how to put everything together.