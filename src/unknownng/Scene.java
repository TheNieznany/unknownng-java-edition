package unknownng;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/** An abstract class for deriving a scene with game loop and stuff. */
public abstract class Scene extends JPanel implements ActionListener {
	/** The frequency of calling the timer in milliseconds. Kind of a fixed framerate thing. */
	protected final int DELAY_MS = 16;

	/** The timer used to update and draw objects periodically. */
	protected Timer timer;

	/** An instance of GraphicsWrapper for drawing. */
	protected GraphicsWrapper gw;



	/** Creates a scene. Don't forget to call it by <code>super(gw)</code>! */
	public Scene(GraphicsWrapper gw) {
		this.gw = gw;

		setPreferredSize(gw.getSizeForPanel());
		setBackground(Color.black);
		setDoubleBuffered(true);
		setFocusable(true);

		timer = new Timer(DELAY_MS, this);
		timer.start();
	}

	/** The game loop controlled by the timer. */
	@Override
	public void actionPerformed(ActionEvent e) {
		update();
		repaint();
	}

	/** A method from Java Swing library. */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		gw.g2d = (Graphics2D)g;

		draw();

		Toolkit.getDefaultToolkit().sync();
	}

	/** Override it and put your update calls there. */
	public abstract void update();

	/** Override it and put your drawing calls there. */
	public abstract void draw();
}
