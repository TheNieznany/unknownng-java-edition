package unknownng;

/**
 * A common implementation for a 2D mathematical vector used throughout the framework.<br>
 * For calculations and storing the values it uses double-precision floating point numbers.<br>
 * For angles and rotations it uses radians.
 */
public class Point {
	/** The location on X-axis. */
	public double x;

	/** The location on Y-axis. */
	public double y;



	/** Creates a point located at (0, 0). */
	public Point() {
		x = 0;
		y = 0;
	}

	/** Creates a point at the specified location. */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/** Creates a point at the same location as the other. */
	public Point(Point other) {
		x = other.x;
		y = other.y;
	}

	/** Checks if the point has the same location as the other. */
	@Override
	public boolean equals(Object object) {
		if (object == null) return false;
		if (object.getClass() != this.getClass()) return false;

		final Point other = (Point)object;
		return (this.x == other.x) && (this.y == other.y);
	}



	/** Returns the counter-clockwise angle between the point and the positive X-axis in radians. */
	public double angle() {
		return Math.atan2(y, x);
	}

	/**
	 * Returns the length between the origin (0, 0) and the point.<br>
	 * Also knowns as the magnitude of a vector.
	 */
	public double length() {
		return Math.sqrt((x * x) + (y * y));
	}

	/**
	 * Returns a new vector that points to the same direction, but its length is 1.<br>
	 * In other words, it's a unit vector.
	 */
	public Point normalized() {
		return new Point(x / length(), y / length());
	}

	/**
	 * Get a vector that points from the current one to the other.<br>
	 * Normalize it to get the direction to the other.<br>
	 * Calculate its length to get the distance between these two points.
	 */
	public Point pointingTo(Point other) {
		return new Point(other.x - x, other.y - y);
	}

	/** Returns a vector that's rotated counter-clockwise around the origin by the specified amount of radians. */
	public Point rotatedBy(double radians) {
		return new Point(
			(Math.cos(radians) * x) - (Math.sin(radians) * y),
			(Math.sin(radians) * x) + (Math.cos(radians) * y)
		);
	}

	/**
	 * Linear interpolation of the current point to the other.
	 * 
	 * @param other The destination point.
	 * @param percentage The percentage of how much to interpolate towards the other point.
	 * 
	 * @return A new point that has been interpolated.
	 */
	public Point lerp(Point other, double percentage) {
		double x = this.x + percentage * (other.x - this.x);
		double y = this.y + percentage * (other.y - this.y);

		return new Point(x, y);
	}
}
