package unknownng;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.Timer;

/** A sprite that is animated. See the tutorial to know how to use it. */
public class AnimatedSprite implements ActionListener {
	/** A helper class for AnimatedSprite, contains data about an animation. */
	public static class Animation {
		protected ArrayList<Rectangle> rects;
		protected int frameAmount;
		protected double frameDuration;

		/** Creates an animation to use with AnimatedSprite. */
		public Animation(Rectangle initialRect, int frameAmount, double frameDuration) {
			rects = new ArrayList<Rectangle>();
			this.frameAmount = frameAmount;
			this.frameDuration = frameDuration;

			for (int i = 0; i < frameAmount; i++) {
				rects.add(new Rectangle(
					initialRect.x + (initialRect.w * i),
					initialRect.y,
					initialRect.w,
					initialRect.h
				));
			}
		}
	}



	/** The width of one frame. */
	public double width;

	/** The height of one frame. */
	public double height;

	private GraphicsWrapper gw;
	private Sprite sprite;
	private Timer timer;

	private HashMap<String, Animation> animations;
	private String currentAnimation;
	private int currentFrame;



	/** Creates an animated sprite. See the tutorial to know how to use it. */
	public AnimatedSprite(GraphicsWrapper gw, String filepath, double width, double height) {
		this.gw = gw;
		this.sprite = new Sprite(gw, filepath);
		this.width = width;
		this.height = height;

		animations = new HashMap<String, Animation>();
		// XXX: Apparently the screen blinks white for a split second
		//      when one doesn't play an animation.
		//      Perhaps it's because of the line under this comment.
		currentAnimation = "";
		currentFrame = 0;
		timer = null;
	}

	/**
	 * Changes to the next frame after the time of the frame duration runs out.<br>
	 * Called by the timer, don't call manually!
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		currentFrame++;

		if (currentFrame == animations.get(currentAnimation).frameAmount) {
			currentFrame = 0;
		}
	}

	/** Draws the current animation frame at the specified location. */
	public void draw(Point point) {
		Rectangle rect = animations.get(currentAnimation).rects.get(currentFrame);
		sprite.drawPart(rect, point);
	}

	/** Adds an animation. */
	public void addAnimation(String name, Animation animation) {
		animations.put(name, animation);
	}

	/** Plays the specified animation. */
	public void playAnimation(String name) {
		if (currentAnimation.equals(name) == false) {
			currentAnimation = name;
			currentFrame = 0;

			if (timer != null) timer.stop();
			timer = new Timer((int)(animations.get(name).frameDuration * 1000), this);
			timer.start();
		}
	}

	/** Returns a new String containing the name of animation that's currently playing. */
	public String getCurrentAnimation() {
		return new String(currentAnimation);
	}
}
