package unknownng;

/**
 * A common implementation for a 2D rectangle used throughout the framework.<br>
 * For calculations and storing the values it uses double-precision floating point numbers.
 */
public class Rectangle {
	/** Upper-left corner location on X-axis. */
	public double x;

	/** Upper-left corner location on Y-axis. */
	public double y;

	/** Width of the rectangle. */
	public double w;

	/** Height of the rectangle. */
	public double h;



	/** Creates a rectangle with specified values. */
	public Rectangle(double x, double y, double w, double h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	/** Creates a copy of the other rectangle. */
	public Rectangle(Rectangle other) {
		x = other.x;
		y = other.y;
		w = other.w;
		h = other.h;
	}

	/** Checks if the rectangle has the same location as the other. */
	@Override
	public boolean equals(Object object) {
		if (object == null) return false;
		if (object.getClass() != this.getClass()) return false;

		final Rectangle other = (Rectangle)object;
		return (this.x == other.x) && (this.y == other.y);
	}



	/** Returns true if the rectangle overlaps with the other. */
	public boolean collidesWith(Rectangle other) {
		return
			x <= (other.x + other.w) &&
			y <= (other.y + other.h) &&
			(x + w) >= other.x &&
			(y + h) >= other.y;
	}

	/** Returns true if the point is inside the rectangle. */
	public boolean hasPoint(Point point) {
		return
			x < point.x &&
			y < point.y &&
			(x + w) > point.x &&
			(y + h) > point.y;
	}

	/** Returns a new point located at the upper-left corner of the rectangle. */
	public Point getXY() {
		return new Point(x, y);
	}
}
