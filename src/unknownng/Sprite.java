package unknownng;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/** A wrapper for BufferedImage class from Java Swing library. */
public class Sprite {
	/** Width of the sprite. */
	public double width;

	/** Height of the sprite. */
	public double height;

	/** Raw BufferedImage object. Used within the framework. */
	BufferedImage data;

	private GraphicsWrapper gw;



	/** Loads an image from the specified file path. */
	public Sprite(GraphicsWrapper gw, String filepath) {
		this.gw = gw;

		try {
			data = ImageIO.read(new File(filepath));
		} catch (IOException e) {
			System.out.println("Error opening image file: " + e.getMessage());
			e.printStackTrace();
		}

		width = data.getWidth();
		height = data.getHeight();
	}

	/** Creates a blank sprite to draw other sprites on. */
	public Sprite(GraphicsWrapper gw, double width, double height) {
		this.gw = gw;
		this.width = width;
		this.height = height;

		data = new BufferedImage((int)width, (int)height, BufferedImage.TYPE_INT_RGB);
	}



	/**
	 * Draws the whole sprite to the screen.
	 *
	 * @param point The location of where to draw the upper-left corner of the sprite.
	 */
	public void draw(Point point) {
		gw.g2d.drawImage(
			this.data,
			(int)(point.x * gw.scale),
			(int)(point.y * gw.scale),
			(int)(this.width * gw.scale),
			(int)(this.height * gw.scale),
			null
		);
	}

	/**
	 * Draws part of the sprite to the screen.
	 *
	 * @param source Draws the area of sprite specified by this rectangle.
	 * @param point The location of where to draw the upper-left corner of the sprite.
	 */
	public void drawPart(Rectangle source, Point point) {
		BufferedImage image = this.data.getSubimage(
			(int)source.x,
			(int)source.y,
			(int)source.w,
			(int)source.h
		);

		gw.g2d.drawImage(
			image,
			(int)(point.x * gw.scale),
			(int)(point.y * gw.scale),
			(int)(image.getWidth() * gw.scale),
			(int)(image.getHeight() * gw.scale),
			null
		);
	}

	/**
	 * Draws a sprite on the current sprite.
	 * 
	 * @param sprite The sprite to draw.
	 * @param point The location of where to draw the upper-left corner of the sprite relative to the current sprite.
	 */
	public void drawOn(Sprite sprite, Point point) {
		Graphics g = this.data.getGraphics();

		g.drawImage(
			sprite.data,
			(int)(point.x),
			(int)(point.y),
			(int)(sprite.width),
			(int)(sprite.height),
			null
		);

		g.dispose();
	}

	/**
	 * Draws part of the sprite on the current sprite.
	 *
	 * @param sprite The sprite to draw.
	 * @param source Draws the area of the sprite specified by this rectangle.
	 * @param point The location of where to draw the upper-left corner of the sprite relative to the current sprite.
	 */
	public void drawPartOn(Sprite sprite, Rectangle source, Point point) {
		BufferedImage image = sprite.data.getSubimage(
			(int)source.x,
			(int)source.y,
			(int)source.w,
			(int)source.h
		);

		Graphics g = this.data.getGraphics();

		g.drawImage(
			image,
			(int)(point.x),
			(int)(point.y),
			(int)(image.getWidth()),
			(int)(image.getHeight()),
			null
		);

		g.dispose();
	}
}
