package unknownng;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * A wrapper for the Graphics2D class from Java Swing library.<br>
 * Makes it convenient to draw the classes from unknownNG framework.<br>
 * Also it scales everything that's drawn on the screen.
 */
public class GraphicsWrapper {
	/** Width of the window. */
	public static double screenWidth;

	/** Height of the window. */
	public static double screenHeight;

	/** Everything that's drawn on the screen is scaled by this value. */
	public static double scale;

	/** Raw Graphics2D object. Used within the framework. */
	Graphics2D g2d;



	/** Creates an instance of this class with specified values. */
	public GraphicsWrapper(double screenWidth, double screenHeight, double scale) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.scale = scale;
	}

	/** Provides screen resolution for the JPanel object. */
	public Dimension getSizeForPanel() {
		return new Dimension((int)(screenWidth * scale), (int)(screenHeight * scale));
	}

	/** Sets the color for drawing primitives. */
	public void setColor(Color color) {
		g2d.setColor(color);
	}

	/** Draws a line from p1 to p2. */
	public void drawLine(Point p1, Point p2) {
		g2d.drawLine(
			(int)(p1.x * scale),
			(int)(p1.y * scale),
			(int)(p2.x * scale),
			(int)(p2.y * scale)
		);
	}

	/** Draws a rectangle outline. */
	public void drawRect(Rectangle rect) {
		g2d.drawRect(
			(int)(rect.x * scale),
			(int)(rect.y * scale),
			(int)(rect.w * scale),
			(int)(rect.h * scale)
		);
	}

	/** Draws a filled rectangle. */
	public void fillRect(Rectangle rect) {
		g2d.fillRect(
			(int)(rect.x * scale),
			(int)(rect.y * scale),
			(int)(rect.w * scale),
			(int)(rect.h * scale)
		);
	}

	/** Draws a simple text string at the specified location. */
	public void drawString(String text, Point point) {
		g2d.drawString(
			text,
			(int)(point.x * scale),
			(int)(point.y * scale)
		);
	}

}
